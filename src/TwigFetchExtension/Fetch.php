<?php

namespace Drupal\twig_fetch_extension\TwigFetchExtension;

use Twig_Extension;
use Twig_Function_Method;

class Fetch extends Twig_Extension
{

  public function fetch ($url, $params=array())
  {
    if($url){
      try {
        $ch = curl_init($url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        if(count($params)>0){
          curl_setopt($ch,CURLOPT_POST,1);
          curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
        }
        $data = curl_exec($ch);
        curl_close($ch);
      }
      catch (Exception $e) {
        return "Connection failed.";
      }
      return $data;
    }
  }
  public function getFunctions ()
  {
    return array(
      'fetch' => new Twig_Function_Method($this, 'fetch', array('is_safe' => array('html')))
    );
  }
  public function getName ()
  {
    return 'twig_fetch_extension';
  }
}
