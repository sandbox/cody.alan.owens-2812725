This module currently just adds the Fetch function to Twig. This function is pulled verbatim and tweaked from
https://gist.github.com/hasinhayder/9705763 to work as a D8 module. It can be invoked in Twig by calling
{{ fetch(url, parameters) }} where parameters is an optional key:value array of URL parameters to be used in the request.